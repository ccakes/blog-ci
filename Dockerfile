FROM ccakes/perl-runtime
ARG BUILD_REF

ADD blog-${BUILD_REF}.tar.gz /blog
WORKDIR /blog

RUN carton install --cached

EXPOSE 3000

CMD carton exec -- morbo script/blog
