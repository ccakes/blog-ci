use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('Blog');

$t->post_ok('/posts' => form => {title => 'Test Case', body => 'Test Body'})
  ->status_is(302);

my $post = $t->tx->res->headers->location;

$t->get_ok($post)
  ->status_is(200)
  ->content_like(qr/Test Case/);

$t->delete_ok($post)
  ->status_is(302);

done_testing;
