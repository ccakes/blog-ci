use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('Blog');

$t->post_ok('/posts' => form => {title => 'Edit', body => 'abcd'})
  ->status_is(302);

my $post = $t->tx->res->headers->location;

$t->get_ok($post)
  ->status_is(200)
  ->content_like(qr/abcd/)
  ->content_unlike(qr/efgh/);

$t->put_ok($post => form => {title => 'Edit', body => 'efgh'})
  ->status_is(302);

$t->get_ok($post)
  ->status_is(200)
  ->content_like(qr/efgh/)
  ->content_unlike(qr/abcd/);

$t->delete_ok($post)
  ->status_is(302);

done_testing;
